5x4 in lego
==================

  ![M20 5x4](pics/m20_v1.jpg)
  ![M20 5x4](pics/m20_v2.jpg)


an ortholinear keyboard set in lego 5x4 with optional encoders, leds and oled

this is part of a bigger family of ortholinear keyboards in lego see for reference
https://alin.elena.space/blog/keeblego/

current status and more

https://gitlab.com/m-lego/m65

kicad symbols/footprints are in the m65 repo above

status:  tested all ok

* [x] gerbers designed
* [x] firmware
* [x] breadboard tested
* [x] gerbers printed
* [x] board tested

Features:

* 5x4
* 1 encoder, optional
* led strip, optional
* 5 pins MX or choco v2 switches
* Raspberry pico 2040 or compatible clones
* oled, optional
* firmware qmk
* tht or surface mount diodes


the pcb
-------

3d render

 ![M20 pcb front](pics/m20-pcb.png)


printed one

  ![M20 pcb front](pics/m20_pcb_front.jpg)
  ![M20 pcb bottom](pics/m20_pcb_bottom.jpg)



parts
----

* 1 RP2040 or compatible, we act, teenstar
* 20 signal diodes 1N4148 , do-35 or sod123 (updated gerbers to support them)
* 1 encoders
* 1x510Ω - for led so you may have to compute the R to match your colours and desired brightness.
* 1 leds
* 2x20 pin DIL/DIP sockets whatever you prefer and headers to match
* led strip 3pins
* 5 pin MX switches 20 or kailh choco v2
* lego 16x16 plates for bottom, and bricks/tiles as you please


repo
----

gerbers and kicad files in here  https://gitlab.com/m-lego/m20/

firmware
--------

in firmware repo


other pictures
--------------

![M20 5x4](pics/m20_two.jpg)
![M20 5x4](pics/m20_lego.jpg)
![M20 5x4](pics/m20_lego-2.jpg)
